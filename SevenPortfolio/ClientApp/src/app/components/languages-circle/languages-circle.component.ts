import { AfterContentChecked, Component, Input, OnInit } from '@angular/core';
import { Language } from 'src/app/models/language';

@Component({
  selector: 'app-languages-circle',
  templateUrl: './languages-circle.component.html',
  styleUrls: ['./languages-circle.component.scss']
})
export class LanguagesCircleComponent implements AfterContentChecked {

  @Input() languages: Language[];
  @Input() radius: number; // in pixels
  @Input() iconSize: number; // in pixels
  @Input() spaceBetweenIcons: number; // in pixels

  startDegree = 90;
  pixelsPerDegree = 0;
  startDegreesOffset = 0;

  constructor() { }

  ngAfterContentChecked() {
    const diameter = this.radius * 2;
    const circleLength = diameter * Math.PI;

    this.pixelsPerDegree = circleLength / 360;
    const halfCircle = circleLength / 2;
    const iconsLengthPx = this.iconSize * this.languages.length;
    const spacesLengthPx = this.spaceBetweenIcons * ( this.languages.length - 1);
    const iconsRadiusZone = 0;
    const takenSpace = iconsLengthPx + spacesLengthPx + iconsRadiusZone;

    let offset = 0;

    if (takenSpace > halfCircle) {
      offset = - (takenSpace - halfCircle) / 2;
    } else {
      offset = (halfCircle - takenSpace) / 2;
    }

    this.startDegreesOffset = +(offset / this.pixelsPerDegree).toFixed(3);
   }

   calculatePosition(index: number) {

    let itemOffsetDegrees = 0;

    const spacesLengthPx = this.spaceBetweenIcons * index;
    const iconsRadiusZone = this.iconSize * index;
    const positionInSectorPx = iconsRadiusZone + spacesLengthPx;
    itemOffsetDegrees = + (positionInSectorPx / this.pixelsPerDegree).toFixed(3);

    const currentStartDegree = +(this.startDegree + this.startDegreesOffset + itemOffsetDegrees + (this.iconSize / 2)).toFixed(3);
    const rotate = currentStartDegree;
    const rotateReverse = rotate * -1;
    const imgShift = this.iconSize / 2;

    const obj = {
      transform: `rotate(${rotate}deg) translate(${this.radius}px) rotate(${rotateReverse}deg)`,
      top: `${-imgShift}px`,
      left: `${-imgShift}px`
    };

    return obj;
  }

}
