import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Language } from 'src/app/models/language';
import { UserInfo } from 'src/app/models/user-info';
import { EditAvatarDialogComponent } from '../../dialogs/edit-avatar-dialog/edit-avatar-dialog.component';
import { EditUserDialogComponent } from '../../dialogs/edit-user-dialog/edit-user-dialog.component';

// uses star rating from:
// https://developer.aliyun.com/mirror/npm/package/levon-angular-star-rating

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss'],
})
export class UserCardComponent {

  // Example data:
  user: UserInfo = {
    id: 1,
    fullName: 'Cristofer Carder Junior',
    avatarUrl: '/assets/avatars/avatar-happy.jpg',
    about: 'UI\/UX Designer, Web Designer, Mobile App Designer, UI\UX Designer, Web Designer, Mobile App Designer',
    address: 'Lviv, Ukraine',
    rating: 4.5,
    reviewsCount: 234,
    languages: [
      { id: 1, name: 'Spanish', icon: '/assets/icons/flags/spanish.svg' },
      { 'id': 2, 'name': 'Turkish', 'icon': '/assets/icons/flags/turkish.svg' },
      { 'id': 3, 'name': 'French', 'icon': '/assets/icons/flags/french.svg' },
      { 'id': 4, 'name': 'English', 'icon': '/assets/icons/flags/english.svg' },
      { 'id': 5, 'name': 'Ukrainian', 'icon': '/assets/icons/flags/ukrainian.svg' },
      { 'id': 6, 'name': 'Russian', 'icon': '/assets/icons/flags/russian.svg' },
      { 'id': 7, 'name': 'German', 'icon': '/assets/icons/flags/german.svg' },
      { 'id': 8, 'name': 'Chinese', 'icon': '/assets/icons/flags/chinese.svg' },
      { 'id': 9, 'name': 'Portuguese', 'icon': '/assets/icons/flags/portuguese.svg' }
    ] as Language[],
  };

  constructor(public dialog: MatDialog) {
    this.user.languages = this.user.languages.reverse();
  }

  openEditUserDialog(): void {

    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      width: '500px',
      data: this.user
    });

    dialogRef.afterClosed().subscribe((result: UserInfo) => {
      // after dialog closed - update data
      if (result) {
        this.user.fullName = result.fullName;
        this.user.about = result.about;
        this.user.address = result.address;
        this.user.languages = result.languages.reverse();
      }

    });

  }

  openEditAvatarDialog(): void {

    const dialogRef = this.dialog.open(EditAvatarDialogComponent, {
      width: '500px',
      data: this.user.avatarUrl
    });

    dialogRef.afterClosed().subscribe((result: string) => {
      if (result) {
        this.user.avatarUrl = result;
      }
    });

  }
}
