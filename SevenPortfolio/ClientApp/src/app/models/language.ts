export interface Language {
    id: number;
    name: string;
    icon: string;
}
