import { Language } from './language';

export interface UserInfo {
    id: number;
    fullName: string;
    avatarUrl: string;
    about: string;
    address: string;

    rating: number;
    reviewsCount: number;
    languages: Language[];
}
