import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Language } from 'src/app/models/language';
import { UserInfo } from 'src/app/models/user-info';

import languages from '../../data/languages.json';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent {

  public user: UserInfo;
  form: FormGroup;

  languagesList: Language[] = languages;

  constructor(
    public dialogRef: MatDialogRef<EditUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserInfo
  ) {

    this.user = data;

    // setup form validations:

    this.form = new FormGroup({
      fullName: new FormControl(this.user.fullName, [
        Validators.required,
        Validators.minLength(3),
      ]),
      about: new FormControl(this.user.about, [
        Validators.required
      ]),
      address: new FormControl(this.user.address, [
        Validators.required,
        Validators.minLength(3),
      ]),
      languages: new FormControl(this.user.languages, [
        Validators.required,
      ])
    });

  }

  langComparer(o1: Language, o2: Language) {
    return o1.id === o2.id;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
