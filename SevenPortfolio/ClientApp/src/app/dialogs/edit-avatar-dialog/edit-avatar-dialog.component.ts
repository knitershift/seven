import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-edit-avatar-dialog',
  templateUrl: './edit-avatar-dialog.component.html',
  styleUrls: ['./edit-avatar-dialog.component.scss']
})
export class EditAvatarDialogComponent implements OnInit {

  form: FormGroup;
  avatar: string | SafeUrl;

  constructor(
    public dialogRef: MatDialogRef<EditAvatarDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
    private sanitizer: DomSanitizer
  ) {

    this.avatar = data;

    this.form = new FormGroup({
      avatarUrl: new FormControl(this.data, [
        Validators.required,
        Validators.minLength(3),
      ]),
    });

   }

  ngOnInit() {
  }

  avatarInputChange(fileInputEvent: any) {
    if (fileInputEvent.target.files && fileInputEvent.target.files[0]) {
      this.avatar = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(fileInputEvent.target.files[0]));
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
